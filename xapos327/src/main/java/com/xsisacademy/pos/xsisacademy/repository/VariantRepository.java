package com.xsisacademy.pos.xsisacademy.repository;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Variant;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface VariantRepository extends JpaRepository<Variant,Long>{

	@Query(value="select * from variant where is_active = true order by name"
			,nativeQuery = true)   //Native Query / SQl/ Database (tidak case sensisitve) ==
	//case insensitive
	List<Variant> findByVariants();
	
	
	List<Variant> findByIsActive(Boolean isActive);
	
	@Query(value="select v from Variant v where v.isActive = true and v.categoryId =?1")
	List<Variant> findByCategoryId(Long categoryId);
}

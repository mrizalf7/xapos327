package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;


@Controller
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index.html");
		//Ambil data dari database get data from databasedd
//		Sort sort = Sort.by(Sort.Direction.ASC, "createdDate");
		List<Category> listCategory  = this.categoryRepository.findAll();
		//pass data to view
		view.addObject("listCategory",listCategory);
		return view;
	}
    
	@GetMapping("addform")
	public ModelAndView addForm() {
		ModelAndView view = new ModelAndView("category/addform.html");
		
		Category category = new Category();
		view.addObject("category",category);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		//ModelAndView view = new ModelAndView("category/sav")\
		if(!result.hasErrors()) {
			if(category.id==null) {
				category.createdBy ="admin1";
				category.createdDate = new Date();
			}
			else {
				Category tempCategory = this.categoryRepository.findById(category.id).orElse(null);
				if(tempCategory!=null) {
					category.createdBy = tempCategory.createdBy;
					category.createdDate = tempCategory.createdDate;
					category.modifiedBy = "admin1";
					category.modifiedDate = new Date();
				}
			}
			this.categoryRepository.save(category);
			return new ModelAndView("redirect:/category/index");
		}
		else {
			return new ModelAndView("redirect:/category/index");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id ) {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null);
		view.addObject("category",category);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {

		Category category = this.categoryRepository.findById(id).orElse(null);
		
		if(category != null) {
			this.categoryRepository.delete(category);
		}
		return new ModelAndView("redirect:/category/index");
	}
	// Using Api
	@GetMapping("index/api")
	public ModelAndView indexApi() {
		ModelAndView view = new ModelAndView("category/index-api.html");
		return view;
	}
}
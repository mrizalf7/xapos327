package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Product;
import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.ProductRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api")
public class ApiProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> productList = this.productRepository.findByProduct();
			return new ResponseEntity<>(productList,HttpStatus.OK);
		}
		catch (Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("product/productByCategoryId/{categoryId}")
	public ResponseEntity<List<Variant>> getproductBySelectedCategory(@PathVariable("categoryId")Long categoryId){
		try {
			List<Variant> productList = this.variantRepository.findByCategoryId(categoryId);
			return new ResponseEntity<>(productList,HttpStatus.OK);
		}
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.createdBy="rizal";
		product.createdDate= new Date();
		
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<>("Save Data Success",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Data Faled",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable("id") Long id){
		try {
			Optional<Product> product= this.productRepository.findById(id);
			return new ResponseEntity<>(product,HttpStatus.OK);
		}
		
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("product/edit/{id}")
	public ResponseEntity<Object> editProduct(@PathVariable("id") Long id,@RequestBody Product product){
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			product.id = id;
			product.modifiedBy = "rizal";
			product.modifiedDate = new Date();
			product.createdBy = productData.get().createdBy;
			product.createdDate = productData.get().createdDate;
			
			this.productRepository.save(product);
			
			return new ResponseEntity<>("Update Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PutMapping("product/delete/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id) {
		Optional<Product> productData = this.productRepository.findById(id);

		if (productData.isPresent()) {
				Product product = new Product();
				product.id = id;
				product.isActive = false;
				product.variantId = productData.get().variantId;
				//product.setIsActve(false); //Set value pakai fungsi Setter
				//product.getIsActive(); //Get Value pakai fungsi Getter
				product.modifiedBy = "admin";
				product.modifiedDate = new Date();
				product.createdBy = productData.get().createdBy;
				product.createdDate = productData.get().createdDate;
				product.productInitial = productData.get().productInitial;
				product.productName = productData.get().productName;
				
				this.productRepository.save(product);
				return new ResponseEntity<>("Delete Data Success",HttpStatus.OK);
		}
		else {
//			return new ResponseEntity.notFound().build();
			return ResponseEntity.notFound().build();
		}
	}
}
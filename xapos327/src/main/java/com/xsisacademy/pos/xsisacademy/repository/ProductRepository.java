package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product,Long> {
	
	@Query(value = "select * from product where is_active = true order by product_name",nativeQuery = true)
	List<Product> findByProduct();
	// Add FindByIsActive(true)
	List<Product> findByIsActive(Boolean isActive);
	//Menggunakan JpaRepository
	/*
	@Query(value="select p from product p where p.isActive =?1 and p.productInitial =?2 order by p.productName")
	List<Product> findByIsActiveAndInitial(Boolean isActive, String productInitial);
	*/
	
	//Menggunakan 2 paramter cth pakai java class
	//@Query(value = "select p from product p where p.isActive =?1 and p.productInitial like '%?2%' order by p.productName")
	//List<Product> findByIsActiveAndInitial(Boolean isActive, String productInitial)

	
	
}

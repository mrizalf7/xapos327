package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.repository.ProductRepository;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	
	//Using API //
	@GetMapping("index/api")
	public ModelAndView indexApi() {
		ModelAndView view = new ModelAndView("product/index-api.html");
		return view;
	}
	
	
}

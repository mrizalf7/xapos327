package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Long>{

	
	@Query(value="select * from category where is_active = true order by category_name"
			,nativeQuery = true)   //Native Query / SQl/ Database (tidak case sensisitve) ==
	//case insensitive
	List<Category> findByCategories();
	
	@Query(value="Select c from Category c where c.isActive = true") //Pakai Java Class Category class harus sama
	//(case sensitive nama class)
	List<Category> findByCategoriesNonNativeQuery();
}
package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/kalkulator")
public class KalkulatorController {
	
	//Using API //
	@GetMapping("/index")
	public ModelAndView indexApi() {
		ModelAndView view = new ModelAndView("kalkulator.html");
		return view;
	}
	
}

package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api")
public class ApiVariantController {

	@Autowired
	private VariantRepository variantRepository;
	
	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant(){
		try {
			List<Variant> listVariant= this.variantRepository.findAll();
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("variant/dynamic_is_active")
	public ResponseEntity<List<Variant>> getDynamicVariant(){
		try {
			List<Variant> listVariant= this.variantRepository.findByIsActive(true);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@PostMapping("variant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {

		variant.createdBy = "admin1";
		variant.createdDate = new Date();

		Variant variantData = this.variantRepository.save(variant);

		if (variantData.equals(variant)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);

		} else {
			return new ResponseEntity<>("Save Data Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("variant/{id}")
	public ResponseEntity<Object> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			return new ResponseEntity<>(variant,HttpStatus.OK);
		}
		
		catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("variant/edit/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id,@RequestBody Variant variant){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		
		if(variantData.isPresent()) {
			variant.id = id;
			variant.modifiedBy = "admin1";
			variant.modifiedDate = new Date();
			variant.createdBy = variantData.get().createdBy;
			variant.createdDate = variantData.get().createdDate;
			
			this.variantRepository.save(variant);
			
			return new ResponseEntity<>("Update Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("variant/delete/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id) {
		Optional<Variant> variantData = this.variantRepository.findById(id);

		if (variantData.isPresent()) {
				Variant variant = new Variant();
				variant.id = id;
				variant.isActive = false;
				variant.categoryId = variantData.get().categoryId;
				//variant.setIsActve(false); //Set value pakai fungsi Setter
				//variant.getIsActive(); //Get Value pakai fungsi Getter
				variant.modifiedBy = "admin";
				variant.modifiedDate = new Date();
				variant.createdBy = variantData.get().createdBy;
				variant.createdDate = variantData.get().createdDate;
				variant.variantInitial = variantData.get().variantInitial;
				variant.variantName = variantData.get().variantName;
				
				this.variantRepository.save(variant);
				return new ResponseEntity<>("Delete Data Success",HttpStatus.OK);
		}
		else {
//			return new ResponseEntity.notFound().build();
			return ResponseEntity.notFound().build();
		}
	}
}